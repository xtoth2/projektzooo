//
// Created by david on 17. 12. 2019.
//

#ifndef UNTITLED9_HRA_H
#define UNTITLED9_HRA_H

#include "Sklad.h"
#include "Obchod.h"

class Hra {
public:
    Hra();
void inicializacia();
void prubehHry();
void vybermoznostiNakupu();
void vybermoznostiProdeje();
void prodejVyrobku();
};


#endif //UNTITLED9_HRA_H
