//
// Created by david on 17. 12. 2019.
//

#include "Sklad.h"

Sklad::Sklad(){

}
Surivona* drevo = new Surivona("drevo",10,1);
Surivona* kamen = new Surivona("kamen",15,2);
Surivona* zelezo = new Surivona("zelezo",20,3);
int Sklad::getPocetDreva(){
    m_pocetDreva = drevo->getPocet();
    return m_pocetDreva;
}
void Sklad::addVyrobek(std::string nazev,int cena, int cenaDreva, int cenaKamene, int cenaZeleza) {
 Vyrobek* newVyrobek = new Vyrobek(nazev,cena,cenaDreva,cenaKamene,cenaZeleza);
 m_vyrobky.push_back(newVyrobek);
}


int Sklad::getPocetKamena(){
    m_pocetKamena = kamen ->getPocet();
    return  m_pocetKamena;
}
int Sklad::getPocetZeleza(){
    m_pocetZeleza = zelezo->getPocet();
    return m_pocetZeleza;
}
void Sklad::printInfo(){
    std::cout << "Pocet dreva je: " << getPocetDreva() << std::endl;
    std::cout << "Pocet kamene je: " << getPocetKamena() << std::endl;
    std::cout << "Pocet zeleza je: " << getPocetZeleza() << std::endl;

}
void Sklad::pridejDrevo(int kolik) {
    drevo->setPocet(kolik+drevo->getPocet());

}
int Sklad::getCenuDreva() {
    return drevo->getCena();
}
int Sklad::getCenuKamena(){
    return kamen->getCena();
}
int Sklad::getCenuZeleza(){
    return zelezo->getCena();
}


void Sklad::pridejKamen(int kolik){
    kamen->setPocet(kolik);
}
void Sklad::pridejZelezo(int kolik){
    zelezo->setPocet(kolik);
}
void Sklad::odoberDrevo(int kolik){
    if (kolik>drevo->getPocet()){
        std::cout<<"Nemas tolko dreva."<<std::endl;
    }
    else{
        drevo->setPocet(drevo->getPocet()-kolik);
    }

}
void Sklad::odoberKamen(int kolik){
    if (kolik>kamen->getPocet()){
        std::cout<<"Nemas tolko kamena."<<std::endl;
    }
    else{
        kamen->setPocet(kamen->getPocet()-kolik);
    }
}
void Sklad::odoberZelezo(int kolik){
    if (kolik>zelezo->getPocet()){
        std::cout<<"Nemas tolko zeleza."<<std::endl;
    }
    else{
        zelezo->setPocet(zelezo->getPocet()-kolik);
    }
}