//
// Created by david on 17. 12. 2019.
//

#include "Hra.h"
int mnozstvo,y,x;
Sklad* sklad = new Sklad();
Obchod* obchod = new Obchod();
Hra::Hra() {
}
void Hra::inicializacia() {
    std::cout<<"Zacina nasa hra, toto mas na zaciatok:" <<std::endl;
    obchod->printInfo();
    sklad->printInfo();
}
void Hra::prubehHry() {
    std::cout<< "Zadej 1 pro nakup."<<std::endl;
    std::cout<< "Zadej 2 pro prodej."<<std::endl;
    std::cout<< "Zadej 3 pro prodej vyrobku."<<std::endl;
    std::cin>>x;

    switch (x){
        case 1: {
            vybermoznostiNakupu();

        }
        case 2:{
            vybermoznostiProdeje();
        }
        case 3:{
            prodejVyrobku();
        }
            }

        }


void Hra::vybermoznostiNakupu() {
    std::cout<<"Pro nakup dreva zadej 1."<<std::endl;
    std::cout<<"Pro nakup kamene zadej 2."<<std::endl;
    std::cout<<"Pro nakup zeleza zadej 3."<<std::endl;
    std::cout<<"Pro navrat zadej 4."<<std::endl;
    std::cin>>y;

    switch (y){
        case 1:{

            std::cout<<"Drevo ma cenu " << sklad->getCenuDreva()<<std::endl;
            std::cout<<"Zadaj mnozstvo, ktore chces kupit: "<<std::endl;
            std::cout<<"Ak sa chces vratit zadaj: 0 "<<std::endl;
            std::cin>>mnozstvo;
            if (sklad->getCenuDreva()*mnozstvo > obchod->getFinance()){
                std::cout<<"Nemas dostatok financii." <<std::endl;
                vybermoznostiNakupu();
            }
            else{
                obchod->setFinance(obchod->getFinance()-(mnozstvo*sklad->getCenuDreva()));
                sklad->pridejDrevo(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiNakupu();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();

        }
        case 2:{
            std::cout<<"Kamen ma cenu " << sklad->getCenuKamena()<<std::endl;
            std::cout<<"Zadaj mnozstvo, ktore chces kupit: "<<std::endl;
            std::cout<<"Ak sa chces vratit zadaj: 0 "<<std::endl;
            std::cin>>mnozstvo;
            if (sklad->getCenuKamena()*mnozstvo > obchod->getFinance()){
                std::cout<<"Nemas dostatok financii." <<std::endl;
                vybermoznostiNakupu();
            }
            else{
                obchod->setFinance(obchod->getFinance()-(mnozstvo*sklad->getCenuKamena()));
                sklad->pridejKamen(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiNakupu();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 3:{
            std::cout<<"Zelezo ma cenu " << sklad->getCenuZeleza()<<std::endl;
            std::cout<<"Zadaj mnozstvo, ktore chces kupit: "<<std::endl;
            std::cout<<"Ak sa chces vratit zadaj: 0 "<<std::endl;
            std::cin>>mnozstvo;
            if (sklad->getCenuZeleza()*mnozstvo > obchod->getFinance()){
                std::cout<<"Nemas dostatok financii." <<std::endl;
                vybermoznostiNakupu();
            }
            else{
                obchod->setFinance(obchod->getFinance()-(mnozstvo*sklad->getCenuZeleza()));
                sklad->pridejZelezo(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiNakupu();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 4:{
            prubehHry();
        }

    }
}
void Hra::vybermoznostiProdeje() {
    std::cout << "Pro prodej dreva zadej 1." << std::endl;
    std::cout << "Pro prodej kamene zadej 2." << std::endl;
    std::cout << "Pro prodej zeleza zadej 3." << std::endl;
    std::cout << "Pro navrat zadej 4." << std::endl;
    std::cin >> y;
    switch (y){
        case 1: {

            std::cout << "Drevo ma cenu " << sklad->getCenuDreva() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetDreva()) {
                std::cout << "Nemas dostatok dreva." << std::endl;
                vybermoznostiProdeje();
            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenuDreva()));
                sklad->odoberDrevo(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiProdeje();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 2: {
            std::cout << "Kamen ma cenu " << sklad->getCenuKamena() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetKamena()) {
                std::cout << "Nemas dostatok kamena." << std::endl;
                vybermoznostiProdeje();
            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenuKamena()));
                sklad->odoberKamen(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiProdeje();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 3: {
            std::cout << "Zelezo ma cenu " << sklad->getCenuZeleza() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetZeleza()) {
                std::cout << "Nemas dostatok zeleza." << std::endl;
                vybermoznostiProdeje();
            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenuZeleza()));
                sklad->odoberZelezo(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiProdeje();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 4: {
            prubehHry();
        }

    }
}
void Hra::prodejVyrobku(){

}
