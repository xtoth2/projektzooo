//
// Created by david on 17. 12. 2019.
//

#include "Vyrobek.h"
Vyrobek::Vyrobek(std::string nazev, int cena, int cenaDreva, int cenaKamene, int cenaZeleza){
    m_nazev = nazev;
    m_cena = cena;
    m_cenaDreva = cenaDreva;
    m_cenaKamene = cenaKamene;
    m_cenaZeleza = cenaZeleza;

}
std::string Vyrobek::getNazev(){
    return m_nazev;
}
int Vyrobek::getCena(){
    return m_cena;
}
int Vyrobek::getCenaDreva(){
    return m_cenaDreva;
}
int Vyrobek::getCenaKamene(){
    return m_cenaKamene;
}
int Vyrobek::getCenaZeleza(){
    return m_cenaZeleza;
}