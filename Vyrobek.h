//
// Created by david on 17. 12. 2019.
//

#ifndef UNTITLED9_VYROBEK_H
#define UNTITLED9_VYROBEK_H

#include <iostream>
#include <vector>

class Vyrobek {
std::string m_nazev;
int m_cena;
int m_cenaDreva;
int m_cenaKamene;
int m_cenaZeleza;
public:
    Vyrobek(std::string nazev,int cena, int cenaDreva, int cenaKamene, int cenaZeleza);
    std::string getNazev();
    int getCena();
    int getCenaDreva();
    int getCenaKamene();
    int getCenaZeleza();

};


#endif //UNTITLED9_VYROBEK_H
