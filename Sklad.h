//
// Created by david on 17. 12. 2019.
//

#ifndef UNTITLED9_SKLAD_H
#define UNTITLED9_SKLAD_H

#include "Vyrobek.h"
#include "Surivona.h"
#include <vector>
class Sklad {
    std::vector<Vyrobek*> m_vyrobky;
int m_pocetDreva;
int m_pocetKamena;
int m_pocetZeleza;
public:
    Sklad();
    int getPocetDreva();
    int getPocetKamena();
    int getPocetZeleza();
    void printInfo();
    void pridejDrevo(int kolik);
    void pridejKamen(int kolik);
    void pridejZelezo(int kolik);
    void odoberDrevo(int kolik);
    void odoberKamen(int kolik);
    void odoberZelezo(int kolik);
    int getCenuDreva();
    int getCenuKamena();
    int getCenuZeleza();
    void addVyrobek(std::string nazev,int cena, int cenaDreva, int cenaKamene, int cenaZeleza);
};


#endif //UNTITLED9_SKLAD_H
